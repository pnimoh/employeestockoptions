package utilitiesclass;

import java.math.BigDecimal;
import java.time.LocalDate;

import model.CompanyShare;
import model.PerformanceBonus;
import model.SaleTransaction;
import model.StockOption;
import model.VestOption;

/**
 * The StockOptionFactory class is a utility class for creating stock option objects.
 * @author Prince Nimoh
 *
 */
public class StockOptionFactory
{
	
	/**
	 * A utility method that converts a string of the format YYYYMMDD to a LocalDate object.
	 * @param input a string of the format YYYYMMDD
	 * @return LocalDate a local date object or null if the input String is invalid.
	 */
	public static LocalDate getLocalDate(String input)
	{
		LocalDate localDate = null;
		
		if(input.length() == 8)
		{
			int year = Integer.parseInt(input.substring(0, 4));
			int month = Integer.parseInt(input.substring(4, 6));
			int day = Integer.parseInt(input.substring(6, 8));
			localDate = LocalDate.of(year,month ,day);
		}
		
		return localDate;
	}
	
	/**
	 * A utility method that takes an input String of the format YYYYMMDD,p.
	 * Where p is the stock price.
	 * It returns a CompanyShare object created from the input String or null if the 
	 * input String is not of the correct format.
	 * @param input String of the format YYYYMMDD,p, where p is the stock price.
	 * @return CompanyShare Returns a CompanyShare object.
	 * @throws Exception An exception is thrown if the market price is less than zero.
	 */
	public static CompanyShare createShareFromString(String input) throws Exception
	{
		CompanyShare companyShare = null;
		
		//TODO: Use regular express to validate the input String.
		
		String[] splitData = input.split(",");
		if(splitData.length == 2)
		{
			LocalDate stockDate = StockOptionFactory.getLocalDate(splitData[0]);
			BigDecimal stockPrice = new BigDecimal(splitData[1]);
			
			companyShare = new CompanyShare(stockDate, stockPrice);
		}
		
		return companyShare;
	}
	
	/**
	 * A utility method that takes an input String and CompanyShare object to create a StockOption object.
	 * 
	 * @param splitData Acceptable input String array formats vary depending on the type of StockOption object to be created.
	 * For VestOption, input String Array must be formated as follows: {VEST,Emp,Date,No,Price}
	 * Where VEST is the record type,
	 * Emp is the employee identifier,
	 * Date is the grant date in the format YYYYMMDD,
	 * No is the amount of units vesting, and 
	 * Price is the grant price.
	 * For PerformanceBonus, input String Array must be formated as follows: {PERF,Emp,Date,Multiplier}
	 * Where PERF is the record type,
	 * Emp see above,
	 * Date is the bonus effect date, and
	 * Multiplier is the bonus multiplier.
	 * For SaleTransaction, input String Array must be formated as follows: {SALE,Emp,Date,Units,Price}
	 * Where SALE is the record type,
	 * Emp see above,
	 * Date is the sale date, and
	 * Price is the sale price.
	 * @param share A CompnayShare share object for the StockOption.
	 * @return StockOption Returns a StockOption object.
	 * @throws Exception An exception is thrown if there are illegal inputs to the constructor.
	 */
	public static StockOption createStockOption(String[] splitData, CompanyShare share) throws Exception
	{
		//TODO: Use regular express to validate the input String.
		StockOption stockOption = null;
		
		//Get the record type.
		String recordType = splitData[0];
		
		if(recordType.equalsIgnoreCase("VEST"))
		{
			
			LocalDate grantDate = StockOptionFactory.getLocalDate(splitData[2]);
			int amountOfUnitsVesting = Integer.parseInt(splitData[3]);
			BigDecimal grantPrice = new BigDecimal(splitData[4]);
			
			stockOption = new VestOption(share, grantDate, amountOfUnitsVesting, grantPrice);
		}
		else if(recordType.equalsIgnoreCase("PERF"))
		{
			LocalDate bonusDate = StockOptionFactory.getLocalDate(splitData[2]);
			BigDecimal bonusMultiplier = new BigDecimal(splitData[3]);
			
			stockOption = new PerformanceBonus(share, bonusDate, bonusMultiplier);
		}
		else if(recordType.equalsIgnoreCase("SALE"))
		{
			LocalDate saleDate = StockOptionFactory.getLocalDate(splitData[2]);
			int unitsSold = Integer.parseInt(splitData[3]);
			BigDecimal saletPrice = new BigDecimal(splitData[4]);
			
			stockOption = new SaleTransaction(share, saleDate, unitsSold, saletPrice);
		}
		
		return stockOption;
	}
}
