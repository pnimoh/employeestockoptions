package tests;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import model.CompanyShare;
import model.PerformanceBonus;
import model.SaleTransaction;
import model.VestOption;

/**
 * Test class for the SaleTransaction class
 * @author Prince Nimoh
 *
 */
class SaleTransactionTest
{
	VestOption vestOption;
	CompanyShare share; 
	
	/**
	 * Test method for {@link model.SaleTransaction#calculateGainFromSale()}.
	 * Tests for the case when sale transaction is decorating a vested option.
	 */
	@Test
	final void testCalculateGainFromSale()
	{
		try
		{
			share = new CompanyShare(LocalDate.of(2012, 06, 15), BigDecimal.valueOf(1.0));
			vestOption = new VestOption(share, LocalDate.of(2012, 01, 01), 1000, BigDecimal.valueOf(0.45));
			LocalDate saleDate = LocalDate.of(2012, 04, 02);
			int unitsSold = 500;
			BigDecimal salePrice = BigDecimal.valueOf(1.00);
			
			SaleTransaction testSaleTransaction = new SaleTransaction(share, vestOption, saleDate, unitsSold, salePrice);
			assertEquals(0, BigDecimal.valueOf(275).compareTo(testSaleTransaction.calculateGainFromSale()), "Gain from sale should be equal to 275");
		}
		catch(Exception ex)
		{
			fail(ex.getMessage());
		}
	}
	
	/**
	 * Test method for {@link model.SaleTransaction#calculateGainFromSale()}.
	 * Tests for the case when the number of units to be sold is greater than the number of units vested.
	 */
	@Test
	final void testCalculateGainFromSaleForfeitCase()
	{
		try
		{
			share = new CompanyShare(LocalDate.of(2012, 06, 15), BigDecimal.valueOf(1.0));
			vestOption = new VestOption(share, LocalDate.of(2012, 01, 01), 499, BigDecimal.valueOf(0.45));
			LocalDate saleDate = LocalDate.of(2012, 04, 02);
			int unitsSold = 500;
			BigDecimal salePrice = BigDecimal.valueOf(1.00);
			
			SaleTransaction testSaleTransaction = new SaleTransaction(share, vestOption, saleDate, unitsSold, salePrice);
			assertEquals(0, BigDecimal.valueOf(0).compareTo(testSaleTransaction.calculateGainFromSale()), "Gain from sale should be equal to 0");
		}
		catch(Exception ex)
		{
			fail(ex.getMessage());
		}
	}
	
	/**
	 * Test method for {@link model.SaleTransaction#calculateCashGain()}.
	 * Tests for the case when a sale transaction decorates the vestOption.
	 */
	@Test
	final void testCalculateCashGainVestOptionDecoratedBySaleTransaction()
	{
		try
		{
			share = new CompanyShare(LocalDate.of(2012, 06, 15), BigDecimal.valueOf(1.0));
			vestOption = new VestOption(share, LocalDate.of(2012, 01, 01), 1000, BigDecimal.valueOf(0.45));
			LocalDate saleDate = LocalDate.of(2012, 04, 02);
			int unitsSold = 500;
			BigDecimal salePrice = BigDecimal.valueOf(1.00);
			
			SaleTransaction testSaleTransaction = new SaleTransaction(share, vestOption, saleDate, unitsSold, salePrice);
			assertEquals(0, BigDecimal.valueOf(275).compareTo(testSaleTransaction.calculateCashGain()), "CashGain should be equal to 275");
		}
		catch(Exception ex)
		{
			fail(ex.getMessage());
		}
	}
	
	/**
	 * Test method for {@link model.SaleTransaction#calculateCashGain()}.
	 * Tests for the case when VestOption is decorated by sale then by a bonus multiplier.
	 */
	@Test
	final void testCalculateCashGainVestOptionDecoratedBySaleTransactionDecoratedByBonusMultiplier()
	{
		try
		{
			share = new CompanyShare(LocalDate.of(2013, 06, 15), BigDecimal.valueOf(1.0));
			vestOption = new VestOption(share, LocalDate.of(2012, 01, 01), 1000, BigDecimal.valueOf(0.45));
			LocalDate saleDate = LocalDate.of(2012, 04, 02);
			int unitsSold = 500;
			BigDecimal salePrice = BigDecimal.valueOf(1.00);
			
			SaleTransaction testSaleTransaction = new SaleTransaction(share, vestOption, saleDate, unitsSold, salePrice);
			
			LocalDate validBonusEffectDate = LocalDate.of(2013, 01, 02);
			BigDecimal bonusMultiplier = BigDecimal.valueOf(1.5);
			PerformanceBonus testPerformanceBonus = new PerformanceBonus(share, testSaleTransaction, validBonusEffectDate, bonusMultiplier);
			
			assertEquals(0, BigDecimal.valueOf(412.5).compareTo(testPerformanceBonus.calculateCashGain()), "CashGain should be equal to 412.5");
		}
		catch(Exception ex)
		{
			fail(ex.getMessage());
		}
	}
	
	/**
	 * Test method for {@link model.SaleTransaction#testCalculateGainFromSale()}.
	 * Tests for the case when a VestOption is decorated by a sale decorated by a bonus multiplier.
	 */
	@Test
	final void testCalculateGainFromSaleVestOptionDecoratedBySaleTransactionDecoratedByBonusMultiplier()
	{
		try
		{
			share = new CompanyShare(LocalDate.of(2012, 06, 15), BigDecimal.valueOf(1.0));
			vestOption = new VestOption(share, LocalDate.of(2012, 01, 01), 1000, BigDecimal.valueOf(0.45));
			LocalDate saleDate = LocalDate.of(2012, 04, 02);
			int unitsSold = 500;
			BigDecimal salePrice = BigDecimal.valueOf(1.00);
			
			SaleTransaction testSaleTransaction = new SaleTransaction(share, vestOption, saleDate, unitsSold, salePrice);
			
			
			LocalDate validBonusEffectDate = LocalDate.of(2013, 01, 02);
			BigDecimal bonusMultiplier = BigDecimal.valueOf(1.5);
			PerformanceBonus testPerformanceBonus = new PerformanceBonus(share, testSaleTransaction, validBonusEffectDate, bonusMultiplier);
			
			
			assertEquals(0, BigDecimal.valueOf(275).compareTo(testPerformanceBonus.calculateGainFromSale()), "Gain from sale should be equal to 275");
		}
		catch(Exception ex)
		{
			fail(ex.getMessage());
		}
	}

}
