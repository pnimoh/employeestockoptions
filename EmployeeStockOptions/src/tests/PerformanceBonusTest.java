/**
 * 
 */
package tests;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import model.CompanyShare;
import model.PerformanceBonus;
import model.VestOption;

/**
 * Test class for the PerformanceBonus class
 * @author Prince Nimoh
 */
class PerformanceBonusTest
{
	
	CompanyShare share;
	VestOption vestOptioned;
	PerformanceBonus testPerformanceBonus;
	
	@Rule
	  public ExpectedException exception = ExpectedException.none();
	
	
	/**
	 * Test method for {@link model.PerformanceBonus#PerformanceBonus()}.
	 * Tests the constructor for the case when the bonus multiplier is zero.
	 */
	@Test
	final void testPerformanceBonusConstructorWithZeroBonusMultiplier()
	{
		try
		{
			//Prepare for the tests
			share = new CompanyShare(LocalDate.of(2013, 06, 15), BigDecimal.valueOf(1.5));
			vestOptioned = new VestOption(share, LocalDate.of(2012, 01, 01), 1000, BigDecimal.valueOf(0.5));
			BigDecimal zeroBonusMultiplier = BigDecimal.valueOf(0);
			LocalDate validBonusEffectDate = LocalDate.of(2013, 01, 02);
			
			exception.expect(IllegalArgumentException.class);
			
			//Perform test
			testPerformanceBonus = new PerformanceBonus(share, vestOptioned, validBonusEffectDate, zeroBonusMultiplier);
			
			//If we get here the test has failed.
			fail("Expected an IllegalArgumentExpection to be thrown");
		}
		catch(Exception ex)
		{
			
			exception.expect(IllegalArgumentException.class);
		}
	}
	
	/**
	 * Test method for {@link model.PerformanceBonus#PerformanceBonus()}.
	 * Tests the constructor for the case when the bonus multiplier is negative.
	 */
	@Test
	final void testPerformanceBonusConstructorWithNegativeBonusMultiplier()
	{
		try
		{
			//Prepare for the tests
			share = new CompanyShare(LocalDate.of(2013, 06, 15), BigDecimal.valueOf(1.5));
			vestOptioned = new VestOption(share, LocalDate.of(2012, 01, 01), 1000, BigDecimal.valueOf(0.5));
			BigDecimal negativeBonusMultiplier = BigDecimal.valueOf(-1.5);
			LocalDate validBonusEffectDate = LocalDate.of(2013, 01, 02);
			
			//Perform test
			testPerformanceBonus = new PerformanceBonus(share, vestOptioned, validBonusEffectDate, negativeBonusMultiplier);
			
			//If we get here the test has failed.
			fail("Expected an IllegalArgumentExpection to be thrown");
		}
		catch(Exception ex)
		{
			exception.expect(IllegalArgumentException.class);
		}
	}
	

	/**
	 * Test method for {@link model.PerformanceBonus#calculateCashGain()}.
	 * Tests for the case when a performance bonus class is decorating a vested stock option.
	 */
	@Test
	final void testCalculateCashGainForPerformanceBonusWithValidBonusMultiplier()
	{
		try
		{
			//Prepare for the tests
			share = new CompanyShare(LocalDate.of(2013, 06, 15), BigDecimal.valueOf(1.5));
			vestOptioned = new VestOption(share, LocalDate.of(2012, 01, 01), 1000, BigDecimal.valueOf(0.5));
			LocalDate validBonusEffectDate = LocalDate.of(2013, 01, 02);
			BigDecimal validBonusMultiplier = BigDecimal.valueOf(1.5);
			
			//Create the test object
			testPerformanceBonus = new PerformanceBonus(share, vestOptioned, validBonusEffectDate, validBonusMultiplier);
			
			//Perform the test
			assertEquals(0, BigDecimal.valueOf(1500).compareTo(testPerformanceBonus.calculateCashGain()), "CashGain should be equal to 1500");
			
		}
		catch(Exception ex)
		{
			fail(ex.getMessage());
		}
		
	}
	
	/**
	 * Test method for {@link model.PerformanceBonus#calculateCashGain()}.
	 * Tests for the case when a performance bonus class is decorating a vested stock option.
	 */
	@Test
	final void testCalculateCashGainForPerformanceBonusWithEqualBonusEffectAndShareDates()
	{
		try
		{
			//Prepare for the tests
			LocalDate shareDate = LocalDate.of(2013, 06, 15);
			share = new CompanyShare(shareDate, BigDecimal.valueOf(1.5));
			vestOptioned = new VestOption(share, LocalDate.of(2012, 01, 01), 1000, BigDecimal.valueOf(0.5));
			LocalDate bonusEffectDateEqualToShareDate = LocalDate.of(2013, 06, 15);
			BigDecimal validBonusMultiplier = BigDecimal.valueOf(1.5);
			
			//Create the test object
			testPerformanceBonus = new PerformanceBonus(share, vestOptioned, bonusEffectDateEqualToShareDate, validBonusMultiplier);
			
			//Perform the test
			assertEquals(0, BigDecimal.valueOf(1500).compareTo(testPerformanceBonus.calculateCashGain()), "CashGain should be equal to 1500");
		}
		catch(Exception ex)
		{
			fail(ex.getMessage());
		}
	}
	
	/**
	 * Test method for {@link model.PerformanceBonus#calculateCashGain()}.
	 * Tests for the case when a performance bonus class is decorating a vested stock option.
	 */
	@Test
	final void testCalculateCashGainForPerformanceBonusWithFutureBonusEffectDate()
	{
		try
		{
			//Prepare for the tests
			LocalDate shareDate = LocalDate.of(2013, 06, 15);
			share = new CompanyShare(shareDate, BigDecimal.valueOf(1.5));
			vestOptioned = new VestOption(share, LocalDate.of(2012, 01, 01), 1000, BigDecimal.valueOf(0.5));
			LocalDate futureBonusEffectDate = LocalDate.of(2014, 06, 15);
			BigDecimal validBonusMultiplier = BigDecimal.valueOf(1.5);
			
			//Create the test object
			testPerformanceBonus = new PerformanceBonus(share, vestOptioned, futureBonusEffectDate, validBonusMultiplier);
			
			//Perform the test
			assertEquals(0, BigDecimal.valueOf(1000).compareTo(testPerformanceBonus.calculateCashGain()), "CashGain should be equal to 1000");
		}
		catch(Exception ex)
		{
			fail(ex.getMessage());
		}
	}
	
	/**
	 * Test method for {@link model.PerformanceBonus#getNumberOfUnitsVestingWithMultiplier()}.
	 * Tests the number of units vesting with bonus multiplier when VestOption object is decorated with 
	 * one performance bonus objects.
	 */
	@Test
	final void testGetNumberOfUnitsVestingWithMultiplier()
	{
		try
		{
			//Prepare for the tests
			share = new CompanyShare(LocalDate.of(2013, 06, 15), BigDecimal.valueOf(1.5));
			vestOptioned = new VestOption(share, LocalDate.of(2012, 01, 01), 1000, BigDecimal.valueOf(0.5));
			LocalDate validBonusEffectDate = LocalDate.of(2013, 01, 02);
			BigDecimal validBonusMultiplier = BigDecimal.valueOf(1.5);
			
			//Create the test object
			testPerformanceBonus = new PerformanceBonus(share, vestOptioned, validBonusEffectDate, validBonusMultiplier);
			
			//Perform the test
			assertEquals(1500, testPerformanceBonus.getNumberOfUnitsVestingWithMultiplier(), "Number of units vesting should be equal to 1500");
			
		}
		catch(Exception ex)
		{
			fail(ex.getMessage());
		}
	}
	
	/**
	 * Test method for {@link model.PerformanceBonus#getNumberOfUnitsVestingWithMultiplier()}.
	 * Tests the number of units vesting with bonus multiplier when VestOption object is decorated with 
	 * one performance bonus objects.
	 */
	@Test
	final void testGetNumberOfUnitsVestingWithMultiplierForTwoPerformanceBonusDecorators()
	{
		try
		{
			//Prepare for the tests
			share = new CompanyShare(LocalDate.of(2013, 06, 15), BigDecimal.valueOf(1.5));
			vestOptioned = new VestOption(share, LocalDate.of(2012, 01, 01), 1000, BigDecimal.valueOf(0.5));
			LocalDate validBonusEffectDate = LocalDate.of(2013, 01, 02);
			BigDecimal firstBonusMultiplier = BigDecimal.valueOf(1.5);
			BigDecimal secondBonusMultiplier = BigDecimal.valueOf(1.1);
			
			
			//Create the first test object
			PerformanceBonus firstPerformanceBonus = new PerformanceBonus(share, vestOptioned, validBonusEffectDate, firstBonusMultiplier);
			
			//Create the second test object
			PerformanceBonus secondPerformanceBonus = new PerformanceBonus(share, firstPerformanceBonus, validBonusEffectDate, secondBonusMultiplier);
			
			//Perform the test
			assertEquals(1650, (int)secondPerformanceBonus.getNumberOfUnitsVestingWithMultiplier(), "Number of units vesting should be equal to 1650");
			
		}
		catch(Exception ex)
		{
			fail(ex.getMessage());
		}
	}
}
