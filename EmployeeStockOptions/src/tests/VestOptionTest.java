package tests;

import model.*;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import java.math.BigDecimal;

/**
 * Test class for the VestOption class
 * @author Prince Nimoh
 *
 */
class VestOptionTest {
	
	VestOption testVestOption;
	CompanyShare share; 
	
	/**
	 * Test method for {@link model.VestOption#calculateCashGain()}.
	 * Tests for the case when the option is vested  and the vest date is after the 
	 * date for the share price. Should return a cash gain value.
	 */
	@Test
	final void testCalculateCashGainVestedCase() {

		try
		{
			share = new CompanyShare(LocalDate.of(2012, 06, 15), BigDecimal.valueOf(1.0));
			testVestOption = new VestOption(share, LocalDate.of(2012, 01, 01), 1000, BigDecimal.valueOf(0.45));
			assertEquals(0, BigDecimal.valueOf(550).compareTo(testVestOption.calculateCashGain()), "CashGain should be equal to 550");
		}
		catch(Exception ex)
		{
			fail(ex.getMessage());
		}
	}
	
	/**
	 * Test method for {@link model.VestOption#calculateCashGain()}.
	 * Tests for the case when the option is vested and the vest date is the same as the date
	 * for the share price. Also uses a different value for the grant price. Should return a cash gain value.
	 *  
	 */
	@Test
	final void testCalculateCashGainWhenVestedDateEqualsGivenDate()
	{
		try
		{
			share = new CompanyShare(LocalDate.of(2012, 06, 15), BigDecimal.valueOf(1.0));
			testVestOption = new VestOption(share, LocalDate.of(2012, 06, 15), 1000, BigDecimal.valueOf(0.55));
			assertEquals(0, BigDecimal.valueOf(450).compareTo(testVestOption.calculateCashGain()), "CashGain should be equal to 450");
		}
		catch(Exception ex)
		{
			fail(ex.getMessage());
		}
	}
	
	/**
	 * Test method for {@link model.VestOption#calculateCashGain()}.
	 * Tests for the case when the option is not vested, that is, the vest date is before the date
	 * for the share price.
	 */
	@Test
	final void testCalculateCashGainNotVestedCase() 
	{
		try
		{
			share = new CompanyShare(LocalDate.of(2012, 06, 15), BigDecimal.valueOf(1.0));
			testVestOption = new VestOption(share, LocalDate.of(2013, 01, 01), 1000, BigDecimal.valueOf(0.45));
			assertEquals(0, BigDecimal.valueOf(0.0).compareTo(testVestOption.calculateCashGain()), "CashGain should be equal to 0.0");
		}
		catch(Exception ex)
		{
			fail(ex.getMessage());
		}
	}
	
	/**
	 * Test method for {@link model.VestOption#calculateCashGain()}.
	 * Tests for the case when the option is vested  but has been forfeited.
	 * Forfeited options have zero value.
	 */
	@Test
	final void testCalculateCashGainIfVestedOptionAreForfeitedCase() {

		try
		{
			share = new CompanyShare(LocalDate.of(2012, 06, 15), BigDecimal.valueOf(1.0));
			testVestOption = new VestOption(share, LocalDate.of(2012, 01, 01), 1000, BigDecimal.valueOf(0.45));
			testVestOption.setIsForfeited(true);
			assertEquals(0, BigDecimal.valueOf(0).compareTo(testVestOption.calculateCashGain()), "CashGain should be equal to 0");
		}
		catch(Exception ex)
		{
			fail(ex.getMessage());
		}
	}
	
	
}
