package model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Iterator;


/**
 * The EmployeeRecord class represents a collection of StockOption records for a stock options program.
 * @author Prince Nimoh
 *
 */
public class EmployeeRecord
{

	private String employeeIdentifier;
	private ArrayList<StockOption> stockOptionRecords;
	
	/**
	 * Initializes a new instance of the EmployeeRecord class.
	 * Takes an employee identifier as parameter.
	 * @param employeeIdentifier The employee identifier for this collection of stock option records.
	 */
	public EmployeeRecord(String employeeIdentifier)
	{
		this.employeeIdentifier = employeeIdentifier;
		stockOptionRecords = new ArrayList<StockOption>();
	}
	
	/**
	 * Returns the employee identifier for this instance of the EmployeeRecord class.
	 * @return The employee identifier.
	 */
	public String getEmployeeIdentifier()
	{
		return employeeIdentifier;
	}
	
	/**
	 * Adds a new VestOption record to this instance of the EmployeeRecord.
	 * @param newVestOption A new VestOption record.
	 */
	public void add(StockOption newStockOption)
	{
		if(newStockOption instanceof StockOptionDecorator)
		{
			
			decorateWith((StockOptionDecorator)newStockOption);
		}
		else
		{
			stockOptionRecords.add(newStockOption);
		}
	}
	
	/**
	 * Returns the total cash gain of vested options for this employee record.
	 * @return The total cash gain for this employee record.
	 */
	public BigDecimal calculateTotalCashGain()
	{
		BigDecimal totalCashGain = BigDecimal.valueOf(0.0);
		
		Iterator<StockOption> iterator = stockOptionRecords.iterator();
		
		while(iterator.hasNext())
		{
			totalCashGain = totalCashGain.add(iterator.next().calculateCashGain());
		}
		
		return totalCashGain.setScale(2, RoundingMode.HALF_UP);
	}
	
	/**
	 * Returns the total cash gain of vested options for this employee record.
	 * @return The total cash gain for this employee record.
	 */
	public BigDecimal calculateTotalGainFromSales()
	{
		BigDecimal totalGainFromSales = BigDecimal.valueOf(0.0);
		
		
		Iterator<StockOption> iterator = stockOptionRecords.iterator();
		
		while(iterator.hasNext())
		{
			totalGainFromSales = totalGainFromSales.add(iterator.next().calculateGainFromSale());
		}
		
		
		return totalGainFromSales.setScale(2, RoundingMode.HALF_UP);
	}
	
	/**
	 * Override for toString() method. Returns comma separated values 
	 * for employee identifier, total cash gain, and 
	 * total gain from sales.
	 */
	@Override
	public String toString()
	{
		return employeeIdentifier + ", " + calculateTotalCashGain() + ", " + calculateTotalGainFromSales();
	}
	
	//Private method that decorates all StockOption records in the 
	//EmployeeRecord with a new StockOptionDecorator.
	private void decorateWith(StockOptionDecorator newDecorator)
	{
		StockOptionDecorator newDecoratorCopy; //We need to make a copy before we wrap an object.
		for(int i = 0; i < stockOptionRecords.size(); i++)
		{
			newDecoratorCopy = newDecorator.getCopy(); //Make of copy of the decorator.
			newDecoratorCopy.setStockOptionToDecorate(stockOptionRecords.get(i)); //Decorate the object currently in the record.
			stockOptionRecords.set(i, newDecoratorCopy); //Set the new decorated object to the current index.
		}
	}
}
