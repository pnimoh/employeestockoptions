package model;


/**
 * The StockOptionDecorator class is an abstract class that provides a base for concrete classes 
 * to inherit and implement new behavior for a stock option.
 * @author Prince Nimoh
 */
public abstract class StockOptionDecorator extends StockOption
{
	private StockOption stockOption;
	
	/**
	 * Initializes a new StockOption decorator instance
	 * @param companyShare An instance of the CompanyShare object
	 * @param stockOption An instance of StockOption object
	 */
	protected StockOptionDecorator(CompanyShare companyShare, StockOption stockOption)
	{
		super(companyShare);
		this.stockOption = stockOption;
	}
	/**
	 * Initializes a new StockOption decorator instance
	 * @param companyShare An instance of the CompanyShare object
	 */
	protected StockOptionDecorator(CompanyShare companyShare)
	{
		super(companyShare);
		this.stockOption = null;
	}
	
	/**
	 * Returns the StockOption object for the decorator. 
	 * Note that the object returned at runtime could be another decorator of type StockOptionDecorator.
	 * @return The StockOption object being decorated.
	 */
	public StockOption getStockOption()
	{
		return stockOption;
	}
	
	/**
	 * Sets an instance of the StockOption class as the objected to be decorated by this
	 * StockOptionDecorator instance.
	 * @param instanceToBeDecoratored The StockOption object to be decorated.
	 */
	public void setStockOptionToDecorate(StockOption instanceToBeDecoratored)
	{
		this.stockOption = instanceToBeDecoratored;
	}
	
	/**
	 * Returns the core StockOption object that is being decorated.
	 * @return The core StockOption object for this decorator instance.
	 */
	public StockOption unWrapAllDecorators()
	{
		return (!(stockOption instanceof StockOptionDecorator)) ? stockOption : ((StockOptionDecorator) stockOption).unWrapAllDecorators();
	}
	
	/**
	 * Returns the number of units of vesting with a multiplier if the base object 
	 * being decorated is of type VestOption. Returns zero if the base type is not 
	 * of type VestOption. 
	 * Subclasses that need to modify this behavior may override this method.
	 * @return The amount of units vesting with a multiplier applied if applicable.
	 */
	public double getNumberOfUnitsVestingWithMultiplier()
	{
		double numberOfUnitsVestingWithMultiplier = 0;
		if(stockOption instanceof VestOption)
		{
			numberOfUnitsVestingWithMultiplier =  ((VestOption) stockOption).getAmountOfUnitsVesting();
		}
		else if(stockOption instanceof StockOptionDecorator)
		{
			numberOfUnitsVestingWithMultiplier =  ((StockOptionDecorator) stockOption).getNumberOfUnitsVestingWithMultiplier();
		}
		
		return numberOfUnitsVestingWithMultiplier;
	}
	
	/**
	 * An abstract method that must be implemented by all subclass.
	 * Makes a shallow copy of the object.
	 * @return A shallow copy of this instance.
	 */
	public abstract StockOptionDecorator getCopy();
}
