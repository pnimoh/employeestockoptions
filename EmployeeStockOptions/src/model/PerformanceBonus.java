/**
 * 
 */
package model;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * The PerformanceBonus class represents a decorator class for a VestOption object.
 * Wrapping a StockOption instance with performance decorator instance adds a bonus multiplier to
 * the calculateCashGain method if applicable.
 * @author prince
 *
 */
public class PerformanceBonus extends StockOptionDecorator
{

	private LocalDate dateBonusTakesEffect;
	private BigDecimal bonusMultiplier;
	private final double MINIMUM_VALUE_FOR_BONUS_MULTIPLIER = 1.0;
	
	/**
	 * Initializes a new instances of the PerformanceBonus class.
	 * @param companyShare An instance of the CompanyShare object
	 * @param stockOption An instance of StockOption object
	 * @param dateBonusTakesEffect The date the performance bonus takes effect
	 * @param bonusMultiplier The bonus multiplier for the performance bonus.
	 */
	public PerformanceBonus(CompanyShare companyShare, StockOption stockOption, 
			               LocalDate dateBonusTakesEffect, BigDecimal bonusMultiplier) throws Exception
	{
		super(companyShare, stockOption);
		this.dateBonusTakesEffect = dateBonusTakesEffect;
		setBonusMultiplier(bonusMultiplier);
	}
	
	/**
	 * Initializes a new instances of the PerformanceBonus class.
	 * @param companyShare An instance of the CompanyShare object
	 * @param stockOption An instance of StockOption object
	 * @param dateBonusTakesEffect The date the performance bonus takes effect
	 * @param bonusMultiplier The bonus multiplier for the performance bonus.
	 */
	public PerformanceBonus(CompanyShare companyShare, LocalDate dateBonusTakesEffect, BigDecimal bonusMultiplier) throws Exception
	{
		super(companyShare);
		this.dateBonusTakesEffect = dateBonusTakesEffect;
		setBonusMultiplier(bonusMultiplier);
	}
	
	//Private setter for bonusMultiplier field.
	//Throws an exception if the bonus multiplier is less than one.
	private void setBonusMultiplier(BigDecimal bonusMultiplier) throws Exception
	{
		if(bonusMultiplier.doubleValue() >= MINIMUM_VALUE_FOR_BONUS_MULTIPLIER)
		{
			this.bonusMultiplier = bonusMultiplier;
		}
		else
		{
			throw new IllegalArgumentException("Invalid bonus multiplier. Bonus multiplier should be equal or greater than zero.");
		}
	}
	
	/**
	 * Applies a bonus multiplier to the stock option this instance of PerformanceBonus is decorating.
	 * The date the bonus takes effect must be on or before the date for the market price of the share
	 * for the bonus multiplier to be applied.
	 */
	@Override
	public BigDecimal calculateCashGain()
	{
		BigDecimal cashGain = getStockOption().calculateCashGain();
		LocalDate dateForMarketPriceOfShare = getCompanyShare().getDateForMarketPrice();
		     
		if (dateForMarketPriceOfShare.isAfter(dateBonusTakesEffect) || 
			dateForMarketPriceOfShare.isEqual(dateBonusTakesEffect))
		{
			//cash gain = cash gain from vested option * bonus multiplier
			cashGain = bonusMultiplier.multiply(getStockOption().calculateCashGain());
		}

		return cashGain;
	}
	
	/**
	 * Returns the total gain through the sale of a stock.
	 * This method returns the calculated gain from sale from the StockOption it is decorating.
	 * @return Returns the gains from a sale.
	 */
	@Override 
	public BigDecimal calculateGainFromSale()
	{
		return getStockOption().calculateGainFromSale();
	}
	
	/**
	 * Returns the number of units of vesting with a multiplier if the base object 
	 * being decorated is of type VestOption. Returns zero if the base type is not 
	 * of type VestOption. 
	 * Subclasses that need to modify this behavior may override this method.
	 * @return The amount of units vesting with a multiplier applied if applicable.
	 */
	@Override
	public double getNumberOfUnitsVestingWithMultiplier()
	{
		double numberOfUnitsVestingWithMultiplier = 0;
		if(getStockOption() instanceof VestOption)
		{
			numberOfUnitsVestingWithMultiplier =  bonusMultiplier.doubleValue() * ((VestOption) getStockOption()).getAmountOfUnitsVesting();
		}
		else if(getStockOption() instanceof StockOptionDecorator)
		{
			numberOfUnitsVestingWithMultiplier =  bonusMultiplier.doubleValue() * ((StockOptionDecorator) getStockOption()).getNumberOfUnitsVestingWithMultiplier();
		}
		
		return numberOfUnitsVestingWithMultiplier;
	}

	/**
	 * Returns a copy of this instance of the PerformanceBonus object.
	 * @return A shallow copy of the PerformanceBonus object.
	 */
	@Override
	public PerformanceBonus getCopy() 
	{
		PerformanceBonus newCopy = null;
		
		try
		{
			newCopy =  new PerformanceBonus(getCompanyShare(), getStockOption() ,dateBonusTakesEffect, bonusMultiplier);
		}
		catch(Exception ex)
		{
			//An Exception should never be thrown because we are copying from another object.
		}
		return newCopy;
	}
	

}
