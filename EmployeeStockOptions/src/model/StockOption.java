package model;

import java.math.BigDecimal;

/**
 * The StockOption class represents an instance of the StockOptionDecorator class
 * This class provides a skeletal implementation for StockOptionDecorator classes to inherit from. 
 * @author Prince Nimoh
 *
 */
public abstract class StockOption
{

	private CompanyShare companyShare;
	
	/**
	 * Constructor for the StockOption class
	 * @param companyShare An instance of the company share object
	 */
	protected StockOption(CompanyShare companyShare)
	{
		this.companyShare = companyShare;
	}

	/**
	 * Returns the company share for this stock option.
	 * @return Company share object
	 */
	public CompanyShare getCompanyShare()
	{
		return companyShare;
	}

	/**
	 * Returns the cash gain for the stock option.
	 * Must be implemented by all subclasses of StockOption.
	 * @return
	 */
	public abstract BigDecimal calculateCashGain();
	
	/**
	 * Returns the total gain through the sale of a stock.
	 * Must be implemented by all subclasses of StockOption.
	 * @return Returns the gains from a sale.
	 */
	public abstract BigDecimal calculateGainFromSale();
}
