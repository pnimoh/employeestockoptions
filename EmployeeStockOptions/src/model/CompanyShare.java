package model;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * The CompanyShare class represents a share for a company. An employee of the company may 
 *exercise stock options for this share.
 * @author Prince Nimoh
 */
public class CompanyShare
{

	private LocalDate dateForMarketPrice;
	private BigDecimal marketPrice;
	private final double MINIMUM_VALUE_FOR_MARKET_PRICE = 0.0;
/**
 * Initializes a new CompanyShare object using the market price of the share 
 * and the given date corresponding to the market price
 * @param dateForMarketPrice The date corresponding to the market price.
 * @param marketPrice The market price of the share on the date provided.
 * @throws IllegalArgumentException This constructor throws an illegalArgument Exception if the market price is less than zero.
 */
	public CompanyShare(LocalDate dateForMarketPrice, BigDecimal marketPrice) throws Exception
	{
		this.dateForMarketPrice = dateForMarketPrice;
		setMarketPrice(marketPrice);
		
	}
	
	//Private setter for the market price. Throws an exception if the market price is less than zero.
	private void setMarketPrice(BigDecimal marketPrice) throws Exception
	{
		if(marketPrice.doubleValue() >= MINIMUM_VALUE_FOR_MARKET_PRICE)
		{
			this.marketPrice = marketPrice;
		}
		else
		{
			throw new IllegalArgumentException("Invalid market price. Market price should be equal or greater than zero.");
		}
	}

	/**
	 * Returns the date corresponding to the market price of the share.
	 * @return Date for the market price of the share.
	 */
	public LocalDate getDateForMarketPrice()
	{
		return dateForMarketPrice;
	}

	
	/**
	 * Returns the market price of the share.
	 * @return The market price of the share.
	 */
	public BigDecimal getMarketPrice()
	{
		return marketPrice;
	}
	
	/**
	 * Updates the market price and date for the share.
	 * Individual setters and getters are not provided for updating the market price and date for the share.
	 * This is to help guard against the market price and date going out of sync.
	 * @param marketPrice The date corresponding to the market price.
	 * @param dateForMarketPrice The market price of the share on the date provided.
	 * @throws IllegalArgumentException This method throws an illegalArgument Exception when the market price is less than zero.
	 */
	public void updateShare(BigDecimal marketPrice, LocalDate dateForMarketPrice) throws Exception
	{
		
		this.dateForMarketPrice = dateForMarketPrice;
		setMarketPrice(marketPrice);
	}
}
