package model;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * The VestOption represents a record of an employees stock option
 * that will be vested at a vested date.
 * Vest Options have a cash gain of zero before the vest date.
 * @author Prince Nimoh
 *
 */
public class VestOption extends StockOption
{

	private LocalDate vestDate;
	private int amountOfUnitsVesting;
	private BigDecimal grantPrice;
	private final int MINIMUM_VALUE_FOR_UNITS_VESTING = 1;
	private final double MINIMU_VALUE_FOR_GRANT_PRICE = 0.0;
	private boolean isForfeited;
	//private LocalDate grantDate; //This is field of the vest option that is not required 
	                                //to solve the problem so commented out

	/**
	 * Initializes a new instance of a vest option record. 
	 * This constructor takes the company share, the vest date, the amount of units vesting, and 
	 * the grant price as parameters.
	 * @param companyShare An instance of the company share object.
	 * @param vestDate The date on which the vest option becomes available.
	 * @param amountOfUnitsVesting The number of units of shares in this vest record.
	 * @param grantPrice The favorable price share price for the employee.
	 * @throws This constructor throws an illegalArgument Exception if the amount of units vesting is zero or less or if grant price is less than zero.
	 */
	public VestOption(CompanyShare companyShare ,LocalDate vestDate, int amountOfUnitsVesting, BigDecimal grantPrice) throws Exception
	{
		super(companyShare);
		this.vestDate = vestDate;
		setAmountOfUnitsVesting(amountOfUnitsVesting);
		setGrantPrice(grantPrice);
		isForfeited = false;
	}
	
	/**
	 * Returns a boolean value indicating if this vested option has been forfeited.
	 * The vest option has zero value if it has been forfeited.
	 * @return The forfeit state of the vest option.
	 */
	public boolean getIsForfeited()
	{
		return isForfeited;
	}
	
	/**
	 * Sets the forfeited state of this vest option.
	 * This vest option has zero value if isForfeited is set to true.
	 * @param isForfeited
	 */
	public void setIsForfeited(boolean isForfeited)
	{
		this.isForfeited = isForfeited;
	}
	
	/**
	 * Returns the vest date for this vest option.
	 * @return The vest date for this vest option.
	 */
	public LocalDate getVestDate()
	{
		return vestDate;
	}
	
	/**
	 * Returns the number of units in this vest option.
	 * @return The number of units in this vest option.
	 */
	public int getAmountOfUnitsVesting()
	{
		return amountOfUnitsVesting;
	}
	
	/**
	 * Returns the grant price for this vest option.
	 * @return The grant price for this vest option.
	 */
	public BigDecimal getGrantPrice()
	{
		return grantPrice;
	}
	
	//Private setter for the amountOfUnitsVesting field. 
	//Throws an exception if the amount of units vesting is less than 1.
	private void setAmountOfUnitsVesting(int amountOfUnitsVesting) throws Exception
	{
		if(amountOfUnitsVesting >= MINIMUM_VALUE_FOR_UNITS_VESTING)
		{
			this.amountOfUnitsVesting = amountOfUnitsVesting;
		}
		else
		{
			throw new IllegalArgumentException("Invalid parameter for amount of units vesting. "
					                          + "Amount of units vesting should be greater than or equal to 1");
		}
	}
	
	//Private setter for the grantPrice field.
	//Throws an exception if the grant price is less than zero.
	private void setGrantPrice(BigDecimal grantPrice) throws Exception
	{
		if(grantPrice.doubleValue() >= MINIMU_VALUE_FOR_GRANT_PRICE)
		{
			this.grantPrice = grantPrice;
		}
		else
		{
			throw new IllegalArgumentException("Invalid grant price. Grant price should be equal or greater than zero.");
		}
	}

	/**
	 * Returns the cash gain for a vest option. 
	 * The vest option becomes available on or after the vest date.
	 * It has a zero cash value before the vest date.
	 * @return The calculated cash gain.
	 */
	@Override
	public BigDecimal calculateCashGain()
	{
		BigDecimal cashGain = BigDecimal.valueOf(0.0);
		
		if (!isForfeited && isVested())
		{
			//cash gain = (market price - grant price) * number of units vested
			cashGain = getCompanyShare().getMarketPrice().add(grantPrice.negate())
					   .multiply(BigDecimal.valueOf(amountOfUnitsVesting));
		}
		
		//TODO: Consider rounding before returning

		return cashGain;
	}
	
	/**
	 * Returns whether this VestOption is vested. 
	 * The VestOption is vested on or after the vest date.
	 * @return
	 */
	public boolean isVested()
	{
		boolean isVested = false;
		LocalDate dateForMarketPriceOfShare = getCompanyShare().getDateForMarketPrice();
		
		if(dateForMarketPriceOfShare.isAfter(vestDate) || dateForMarketPriceOfShare.isEqual(vestDate))
		{
			isVested = true;
		}
		return isVested;
	}
	
	/**
	 * Returns the total gain through the sale of a stock.
	 * This method returns zero as VestOption does have any sale behaviour.
	 * @return Returns the gains from a sale.
	 */
	@Override 
	public BigDecimal calculateGainFromSale()
	{
		return BigDecimal.valueOf(0.0);
	}
}
