
package model;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * The SaleTransaction class represents a sale recored for a stock option.
 * @author Prince Nimoh
 *
 */
public class SaleTransaction extends StockOptionDecorator
{
	private LocalDate saleDate;
	private int unitsToSell;
	private BigDecimal salePrice;
	private final int MINIMUM_UNITS_SOLD = 1;
	private final double MINIMUM_VALUE_FOR_SALE_PRICE = 0.0;

	/**
	 * Initializes a new instance of a sale transaction. This constructor takes the
	 * company share, stockOption, saleDate, unitsSold, and salePrice.
	 * 
	 * @param companyShare
	 *            An instance of the company share object.
	 * @param stockOption
	 *            The date on which the vest option becomes available.
	 * @param saleDate
	 *            The date on which the sale transaction occurred.
	 * @param unitsToSell
	 *            The number of units to sell in this transaction.
	 * @param salePrice
	 *            The price for selling the share.
	 */
	public SaleTransaction(CompanyShare companyShare, StockOption stockOption, LocalDate saleDate, int unitsToSell,
			BigDecimal salePrice) throws Exception
	{
		super(companyShare, stockOption);
		this.saleDate = saleDate;
		setUnitsToSell(unitsToSell);
		setSalePrice(salePrice);
	}
	
	/**
	 * Initializes a new instance of a sale transaction. This constructor takes the
	 * company share, saleDate, unitsSold, and salePrice.
	 * 
	 * @param companyShare
	 *            An instance of the company share object.
	 * @param saleDate
	 *            The date on which the sale transaction occurred.
	 * @param unitsToSell
	 *            The number of units to sell in this transaction.
	 * @param salePrice
	 *            The price for selling the share.
	 */
	public SaleTransaction(CompanyShare companyShare, LocalDate saleDate, int unitsToSell,
			BigDecimal salePrice) throws Exception
	{
		super(companyShare);
		this.saleDate = saleDate;
		setUnitsToSell(unitsToSell);
		setSalePrice(salePrice);
	}

	// Private setter for the the unitsSold field.
	// Throws an exception if the number of units sold is 1.
	private void setUnitsToSell(int unitsToSell) throws Exception
	{
		if (unitsToSell >= MINIMUM_UNITS_SOLD)
		{
			this.unitsToSell = unitsToSell;
		} else
		{
			throw new IllegalArgumentException("Invalid parameter for number of units sold. "
					+ "Number of units sold should be greater than or equal to 1");
		}
	}

	// Private setter for the salePrice field.
	// Throws an exception if the salePrice is less than zero.
	private void setSalePrice(BigDecimal salePrice) throws Exception
	{
		if (salePrice.doubleValue() >= MINIMUM_VALUE_FOR_SALE_PRICE)
		{
			this.salePrice = salePrice;
		} else
		{
			throw new IllegalArgumentException("Invalid sale price. Sale price should be equal or greater than zero.");
		}
	}

	/**
	 * Returns the gains through the sale of shares. Only options that are vested
	 * can be sold on the date of the sale can be sold.
	 * 
	 */
	@Override
	public BigDecimal calculateCashGain()
	{
		BigDecimal cashGain = getStockOption().calculateCashGain();
		
		if(isVestedbySaleDate() && !(cashGain.doubleValue() == 0.0))
		{
			cashGain = cashGain.add(calculateGainFromSale().negate());
		}
		
		return cashGain;
	}
	
	/**
	 * Returns the gains through the sale of shares. Only options that are vested 
	 * on or before the date of the sale can be sold.
	 * @return The calculated gain from this sale.
	 */
	@Override
	public BigDecimal calculateGainFromSale()
	{
		BigDecimal totalGainFromSale = getStockOption().calculateGainFromSale();
		VestOption vestOption = getVestOption();
		
		int amountOfUnitsVesting = getNumberOfUnitsVesting();
		
		//Check that the number of units vested is greater than units to be sold.
		//If the number of units vested is less than units to be sold the vested option is forfeited.
		if(unitsToSell > amountOfUnitsVesting)
		{
			vestOption.setIsForfeited(true);
		}
		
		if(vestOption != null && !vestOption.getIsForfeited() && isVestedbySaleDate())
		{
			//Overall formula for this calculation
			//Total gain from sales = (sale price - grant price) * units sold + gains from other sales
			
			//salePrice - grantPrice
			BigDecimal saleAndGrantPriceDifference = salePrice.add(vestOption.getGrantPrice().negate());
			
			//Gains from this sale = unitsSold * (salePrice - grantPrice)
			BigDecimal gainsFromThisSale = BigDecimal.valueOf(unitsToSell).multiply(saleAndGrantPriceDifference);
			
			//Total gains = gains from this sale + gains from other sales
			totalGainFromSale = gainsFromThisSale.add(totalGainFromSale);
		}
		
		return totalGainFromSale;
	}
	
	//Private method that unwraps and returns the VestOption if available.
	private VestOption getVestOption()
	{
		StockOption coreStockOptionObject = this.unWrapAllDecorators();
		return (coreStockOptionObject instanceof VestOption) ? (VestOption)coreStockOptionObject : null;
	}
	
	//Private method for checks if VestOptions are vested by the sale date.
	private boolean isVestedbySaleDate()
	{
		boolean isVested = false;
		
		VestOption vestOption = getVestOption();
		
		if( vestOption != null && saleDate.isAfter(vestOption.getVestDate()) || 
			     saleDate.isEqual(vestOption.getVestDate()))
		{
			isVested = true;
		}
		
		return isVested;
	}
	
	//Private method that gets number of units vesting or vested.
	//It takes into account if there is a bonus multiplier(s).
	private int getNumberOfUnitsVesting()
	{
		VestOption vestOption = getVestOption();
		//Get amount of units vesting from the vest option.
		int amountOfUnitsVesting = vestOption.getAmountOfUnitsVesting();
				
		//if the StockOption object has a decorator, then there may be a multiplier for the amount of units vesting.
		//Therefore, get the amount of units vesting that may have the multiplier
		if(getStockOption() instanceof StockOptionDecorator) //there is a decorator
		{
			amountOfUnitsVesting = (int)((StockOptionDecorator)getStockOption()).getNumberOfUnitsVestingWithMultiplier();
					
		}
		return amountOfUnitsVesting;
	}
	
	/**
	 * Returns a copy of this instance of the SaleTransaction object.
	 * @return A shallow copy of the SaleTransaction object.
	 */
	@Override
	public SaleTransaction getCopy()
	{
		SaleTransaction newCopy = null;
		
		try
		{
			newCopy = new SaleTransaction(getCompanyShare(), getStockOption(), saleDate, unitsToSell, salePrice);
		}
		catch(Exception ex)
		{
			//An Exception should never be thrown because we are copying from another object.
		}
		return newCopy;
	}

}
