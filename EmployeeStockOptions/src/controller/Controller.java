package controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;

import model.CompanyShare;
import model.EmployeeRecord;
import model.StockOption;
import utilitiesclass.StockOptionFactory;


public class Controller
{

	public static void main(String[] args)
	{
		//The model is a TreeMap of Employee records.
		TreeMap<String, EmployeeRecord> records  = new TreeMap<String, EmployeeRecord>();
		
		
		ArrayList<String> input = getInput();
		
		
		if(isValidInput(input))
		{
			//Remove the first item which is not a record.
			input.remove(0);
			
			//Remove the the last item which is the share price information.
			int indexOfStockInfo = input.size() - 1;
			String companyStockInfo = input.remove(indexOfStockInfo);
			
			try
			{
				
				CompanyShare companyShare = StockOptionFactory.createShareFromString(companyStockInfo);
				
				Iterator<String> iterator = input.iterator();
				while(iterator.hasNext())
				{
					String[] splitData = iterator.next().split(",");
					
					String employeeIdentifier = splitData[1];
					StockOption stockOption = StockOptionFactory.createStockOption(splitData, companyShare);
					
					if(stockOption != null)
					{
						//check if the employee is in the tree
						EmployeeRecord employeeRecord = records.get(employeeIdentifier);
						
						//if the employee is not already in our records, lets create and add them.
						if(employeeRecord == null) 
						{
							employeeRecord = new EmployeeRecord(employeeIdentifier);
							records.put(employeeIdentifier, employeeRecord);
						}

						//Now the employee is in our records so lets add the stock option to them.
						employeeRecord.add(stockOption);
					}
				}
				
				//Print records
				printRecords(records);
			}
			catch (Exception ex)
			{
				System.out.println(ex.getMessage());
			}
		}
		else
		{
			System.out.println("Invalid input file format.");
		}
	}
	
	
	//Private method that checks if the input was valid.
	private static boolean isValidInput(ArrayList<String> input)
	{
		boolean isValid = false;
		final int NON_RECORD_INPUTS = 2;
		
		try
		{
			//Get the first item in the input.
			//This is expected to be an integer.
			int numberOfRecords = Integer.parseInt(input.get(0));
			
			if((numberOfRecords + NON_RECORD_INPUTS) == input.size())
			{
				isValid = true;
			}
		}
		catch(NumberFormatException ex)
		{
			return false;
		}
		
		return isValid;
	}
	
	
	//Private method used to get input from stdin.
	private static ArrayList<String> getInput()
	{
		InputStreamReader isReader = new InputStreamReader(System.in);
		BufferedReader bufReader = new BufferedReader(isReader);
		ArrayList<String> input = new ArrayList<String>();
		
		while (true)
		{
			try
			{
				String inputStr = null;
				if ((inputStr = bufReader.readLine()) != null)
				{
					input.add(inputStr);
				} 
				else
				{
					return input; // end on first null
				}
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	//Private method that prints employee records.
	private static void printRecords(TreeMap<String, EmployeeRecord> records)
	{
		for(Entry<String, EmployeeRecord> entry : records.entrySet())
		{
			EmployeeRecord value = entry.getValue();
			System.out.println(value.toString());
		}
	}
}
